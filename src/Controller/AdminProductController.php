<?php

namespace App\Controller;

use App\Entity\Picture;
use App\Entity\Product;
use App\Form\PictureType;
use App\Form\ProductType;
use Doctrine\ORM\Mapping\Entity;
use App\Repository\PictureRepository;
use App\Repository\ProductRepository;
use App\Service\Uploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/admin/product')]
class AdminProductController extends AbstractController
{
    #[Route('/', name: 'app_admin_product_index', methods: ['GET'])]
    public function index(ProductRepository $productRepository): Response
    {
        return $this->render('admin_product/index.html.twig', [
            'products' => $productRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admin_product_new', methods: ['GET', 'POST'])]
    #[Route('/{id}/edit', name: 'app_admin_product_edit', methods: ['GET', 'POST'])]
    public function new(Product $product=null, Request $request, ProductRepository $productRepository, SluggerInterface $sluggerInterface): Response
    {
        if($product==null)
            $product = new Product();

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $product->setSlug($sluggerInterface->slug($product->getName())->lower());
            $productRepository->add($product,true);

            /*$entityManager->persist($product);
            $entityManager->flush();*/

            return $this->redirectToRoute('app_admin_product_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_product/new.html.twig', [
            'form' => $form,
            'edit' => $product->getId()
        ]);
    }

    #[Route('/{id}/pictures', name: 'app_admin_product_pictures', methods: ['GET'])]
    public function listPictures(Product $product): Response
    {
        return $this->render('admin_product/listPictures.html.twig', [
            'product' => $product
        ]);
    }

    /**    
     * #[Route('/{id}/picture/edit/{picture_id}', name: 'app_admin_product_edit', methods: ['GET', 'POST'])]
     * #[Entity('picture', expr: 'repository.find(picture_id)')]
     */

    #[Route('/{id}/picture/new', name: 'app_admin_product_picture_new', methods: ['GET', 'POST'])]
    public function newPicture(Product $product, Request $request, SluggerInterface $sluggerInterface, PictureRepository $pictureRepository, Uploader $uploader): Response
    {
        $picture = new Picture();

        $form = $this->createForm(PictureType::class, $picture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $picture->setSlug($sluggerInterface->slug($picture->getName())->lower());
            $picture->setProduct($product);
            $pictureUpload = $form->get('picture')->getData();
            if ($pictureUpload) {
                $path = $uploader->upload($pictureUpload, 'product');
                $picture->setPath($path);
            }
            $pictureRepository->add($picture, true);

            return $this->redirectToRoute('app_admin_product_pictures', ['id'=> $product->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_product/newPicture.html.twig', [
            'form' => $form,
            'product' => $product,
            'edit' => $picture->getId()
        ]);
    }

    #[Route('/{id}', name: 'app_admin_product_show', methods: ['GET'])]
    public function show(Product $product): Response
    {
        return $this->render('admin_product/show.html.twig', [
            'product' => $product,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_product_delete', methods: ['POST'])]
    public function delete(Request $request, Product $product, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$product->getId(), $request->request->get('_token'))) {
            $entityManager->remove($product);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_admin_product_index', [], Response::HTTP_SEE_OTHER);
    }
}
