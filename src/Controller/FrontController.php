<?php

namespace App\Controller;

use Twig\Environment;
use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FrontController extends AbstractController
{
    #[Route('/', name: 'homepage')]
    public function noLocalHomepage(): Response
    {
        return $this->redirectToRoute('app_front', ['_locale' => 'fr']);
    }

    #[Route('/{_locale<%app.supported_locales%>}', name: 'app_front')]
    public function index(ProductRepository $productRepository): Response
    {

        return $this->render('front/index.html.twig', 
            ['products'=>$productRepository->findBy([],['name'=>'ASC'])]
        );
    }

    #[Route('/{_locale<%app.supported_locales%>}/product/{slug}', name: 'app_detail_product')]
    public function detailProduct(Product $product): Response
    {

        return $this->render(
            'front/product.html.twig',
            ['product' => $product ]
        );
    }

    #[Route('/{_locale<%app.supported_locales%>}/pages/{page}', name: 'app_pages')]
    public function pages(string $_locale, string $page=null, Environment $twig): Response
    {
        $template = 'front/pages/' . $page . '.' . $_locale . '.html.twig';
        $loader = $twig->getLoader();
        if (!$loader->exists($template))
            throw new NotFoundHttpException();
            
        return $this->render($template, [
            'controller_name' => 'FrontController',
        ]);
    }
}
