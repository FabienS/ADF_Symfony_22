<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Tva;
use DateTimeImmutable;
use App\Entity\Picture;
use App\Entity\Product;
use App\Entity\Category;
use App\Repository\TvaRepository;
use App\Repository\CategoryRepository;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\String\Slugger\SluggerInterface;

class AppFixtures extends Fixture
{
    private $sluggerInterface;

    public function __construct(SluggerInterface $sluggerInterface)
    {
        $this->sluggerInterface = $sluggerInterface;
        
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        $tva = new Tva();
        $tva->setName('20')->setRate('1.2');

        $manager->persist($tva);


        for($i=0;$i<10;$i++) {
            $category = new Category();
            $category->setTitle($faker->word())
            ->setSlug($this->sluggerInterface->slug($category->getTitle())->lower());

            $manager->persist($category);

            for($j=0;$j < 10; $j++) {

              
                // generate data by calling methods
                echo $faker->name();
                // 'Vince Sporer'
                echo $faker->email();
                // 'walter.sophia@hotmail.com'
                echo $faker->text();
                // 'Numquam ut mollitia at consequuntur inventore dolorem.'
                $product = new Product();
                $product->setName($faker->word())
                ->setPrice($faker->randomFloat(2))
                ->setCategory($category)
                ->setSlug($this->sluggerInterface->slug($product->getName())->lower())
                ->setCreatedAt(new DateTimeImmutable())
                ->setQuantityInStock($faker->randomNumber(3, false))
                ->setPublishedAt(new DateTimeImmutable())
                ->setDescription($faker->paragraph(2))
                ->setSku($faker->ean13())
                ->setTva($tva);
                $manager->persist($product);

                for($k=0;$k<5;$k++){
                    $productPicture = new Picture();
                    $productPicture->setName($faker->word())
                    ->setDescription($faker->paragraph(2))
                    ->setSlug($this->sluggerInterface->slug($productPicture->getName())->lower())
                    ->setProduct($product)
                    ->setPath('https://loremflickr.com/1024/768/face,people,portait?lock='.($faker->randomNumber(5)));
                    $manager->persist($productPicture);
                }
            }


        }

        $manager->flush();
    }
}
