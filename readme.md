# Ecommerce Symfony


1. Vérifiez les pré-requis (php > 8, mysql, composer, symfony.exe)
2. Installation de Symfony à la racine du dossier choisi ! (regarder la doc)
3. Création de la configuration locals (.env.local) : et création database
4. Démarrage server Symfony : on test l'accès à la première page
5. Création du premier controller (page d'accueil /)
6. Création de nos entités à partir du MCD ou du diagramme de classe
7. Relation entre nos entités
8. Migration vers la base de données ou Mise à jour du schéma
9. Création de nos controllers admin (prefixer avec Admin : exemple : AdminProductController) pour créer de la donnée (CRUD). ¨Product - Category - Tva
10. Eventuellement faire des Fixtures (création de jeux de données fake ou pas)
11. Petites choses en plus :
    1.  Modifier le Type (Form) : définir le type des champs nécessaire
    2.  Gérer le slug
    3.  Créer le layout de l'admin et l'associer aux vues admin
    4.  Ajouter boostrap à l'admin et form twig avec bootstrap
    5.  Ajouter une navigation à l'admin
    6.  Optimiser l'ajout et l'édit dans les controller CRUD
12. Upload des images
13. Afficher sur le Front nos produits (sur la page front d'accueil)
14. Page de détails produits
15. Ajouter la gestion du multilangue (en|fr)
16. Ajouter une méthode Front pour la gestion des pages statiques (presentation mentions légales, contacts...)
17. Création du register client
18. Création du login client
19. Et après :
    1.  Ajout au panier (session) 
    2.  Validation de la commande
    3.  Paiement !